
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
	 ["#212526" "#ff4b4b" "#b4fa70" "#fce94f" "#729fcf" "#e090d7" "#8cc4ff" "#eeeeec"])
 '(custom-enabled-themes (quote (tango-dark-2)))
 '(custom-safe-themes
	 (quote
		("a2501609612de7a411468414710bce6428b7be8b89e347083f71978236caa44b" default)))
 '(package-selected-packages (quote (markdown-mode org)))
 '(safe-local-variable-values
	 (quote
		((eval add-hook
					 (quote after-save-hook)
					 (lambda nil
						 (org-babel-tangle))
					 nil t)
		 (eval load-file "backend.el")))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
