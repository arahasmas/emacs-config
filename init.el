;;;; init.el ;;;;
;;
;; Araha Smas
;; arahasmas at gmx dot com
;; 2019-10-26
;;
;; This file was generated with Emacs org-mode.
;; The original source file was emacsinit.org.
;;
;;;;

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file)

(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
										(not (gnutls-available-p))))
			 (proto (if no-ssl "http" "https")))
	(when no-ssl
		(warn "\
Your version of Emacs does not support SSL connections,
which is unsafe because it allows man-in-the-middle attacks.
There are two things you can do about this warning:
1. Install an Emacs version that does support SSL and be safe.
2. Remove this warning from your init file so you won't see it again."))
	;; Comment/uncomment these two lines to enable/disable MELPA and
	;; MELPA Stable as desired
	;;(add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
	(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
	(when (< emacs-major-version 24)
		;; For important compatibility libraries like cl-lib
		(add-to-list 'package-archives (cons "gnu" (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)

(add-to-list 'load-path
							"~/.emacs.d/plugins/yasnippet")
(require 'yasnippet)
(yas-global-mode 1)
(put 'scroll-left 'disabled nil)

(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-switchb)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
	 (python . t)
	 (shell . t)
	 (css . t)))

(setq org-src-fontify-natively t)

(setq org-confirm-babel-evaluate nil)

(add-hook 'org-mode-hook 'turn-on-auto-fill)

(setq markdown-command "pandoc")

;; (setq markdown-command-needs-filename nil)

;; (setq markdown-open-command nil)

(setq markdown-hr-strings
			(list
			 "-------------------------------------------------------------"
			 "* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"
			 "---------------------------------"
			 "* * * * * * * * * * * * * * * * *"
			 "---------"
			 "* * * * *"))

;; (setq markdown-bold-underscore nil)

(setq markdown-italic-underscore t)

;; (setq markdown-asymmetric-header nil)

(setq markdown-header-scaling t)

(setq markdown-header-scaling-values '(1.25 1.2 1.15 1.1 1.05 1.0))

;; (setq markdown-list-indent-width 4)

;; (setq markdown-indent-function 'markdown-indent-line)

(setq markdown-indent-on-enter 'indent-and-new-item)

(setq markdown-enable-wiki-links t)

;; (setq markdown-wiki-link-alias-first t)

;; (setq markdown-uri-types (list
;; 													 "acap" "cid" "data" "dav" "fax" "file" "ftp"
;;	  												 "gopher" "http" "https" "imap" "ldap" "mailto"
;; 													 "mid" "message" "modem" "news" "nfs" "nntp"
;; 													 "pop" "prospero" "rtsp" "service" "sip" "tel"
;; 													 "telnet" "tip" "urn" "vemmi" "wais"))

;; (setq markdown-enable-math nil)

;; (setq markdown-css-paths nil)

;; (setq markdown-content-type "")

;; (setq markdown-coding-system nil)

;; (setq markdown-xhtml-header-content "")

;; (setq markdown-xhtml-standalone-regexp "^\\(<\\?xml\\|<!DOCTYPE\\|<html\\)")

;; (setq markdown-link-space-sub-char "_")

;; (setq markdown-reference-location 'header)

(setq markdown-footnote-location 'immediately)

;; (setq markdown-nested-imenu-heading-index t)

;; (setq comment-auto-fill-only-comments nil)

;; (setq markdown-gfm-additional-languages nil)

;; (setq markdown-gfm-use-electric-backquote t)

;; (setq markdown-make-gfm-checkboxes-buttons t)

;; (setq markdown-hide-urls nil)

;; (setq markdown-hide-markup nil)

(setq markdown-fontify-code-blocks-natively t)

(setq markdown-gfm-uppercase-checkbox t)

(setq scroll-preserve-screen-position t)

(setq scroll-step 1)

(setq scroll-conservatively 10000)

(setq mouse-wheel-scroll-amount '(2 ((shift) . 2)))
(setq mouse-wheel-progressive-speed t)
(setq mouse-wheel-follow-mouse 't)

(setq next-screen-context-lines 1)

(setq fast-but-imprecise-scrolling t)

(setq recenter-position '(middle bottom top))

(setq hscroll-step 1)

(setq scroll-margin 2)

(setq show-trailing-whitespace t)

(put 'narrow-to-region 'disabled nil)

(setq make-backup-files t)

(setq backup-directory-alist '(("." . "~/.emacs.d/backups/")))

(setq version-control t)

(setq kept-old-versions 500)
(setq kept-new-versions 500)

(setq delete-old-versions t)

(setq require-final-newline nil)

(add-hook 'before-save-hook 'time-stamp)

(setq auto-save-timeout 30)

(setq fill-column 72)

(setq standard-indent 2)

(setq-default tab-width 2)

(setq inhibit-startup-screen t)

(setq initial-scratch-message
";; F5 emacsinit.org | C-F5 reload config file | eval-buffer | eval-region
;; C-M-x eval-defun | C-c C-e eval-expression | C-j eval-print-last-sexp")

(setq mark-even-if-inactive nil)

(setq mark-ring-max 500)
(setq global-mark-ring-max 500)
(setq kill-ring-max 1800)
(setq undo-limit 240000)
(setq undo-strong-limit 360000)
(setq undo-outer-limit 12000000)
(setq kmacro-ring-max 24)
(setq large-file-warning-threshold 10000000)

(setq kill-read-only-ok t)

(setq register-preview-delay 0.5)

(setq use-dialog-box nil)

(fset 'yes-or-no-p 'y-or-n-p)

(set-frame-position nil 0 0)
(set-frame-size (selected-frame) 86 48)

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)

(auto-fill-mode 1)

(delete-selection-mode 1)

(show-paren-mode 1)

(scroll-bar-mode -1)

(tool-bar-mode -1)

(menu-bar-mode -1)

(column-number-mode 1)

(defun my/scroll-down (&optional n)
	"Scroll down the current buffer by 1 row.
If called with the N argument scroll down by N rows. It can be called
interactively with C-u or M-[number]"
	(interactive "p")
	(scroll-down (or n 1)))

(global-set-key (kbd "M-<next>") 'my/scroll-down)

(defun my/scroll-up (&optional n)
	"Scroll up the current buffer by 1 row.
If called with the N argument scroll up by N rows. It can be called
interactively with C-u or M-[number]"
	(interactive "p")
	(scroll-up (or n 1)))

(global-set-key (kbd "M-<prior>") 'my/scroll-up)

(defun my/scroll-down-freezed-cursor (&optional n)
	"Scroll down the buffer by 1 row keeping the cursor on the same line.
If called with the N argument scroll down by N rows. It can be called
interactively with C-u or M-[number]"
	(interactive "p")
	(let ((number-of-lines (or n 1)))
		(scroll-down number-of-lines)
		(forward-line (- number-of-lines))))

(global-set-key (kbd "S-M-<next>") 'my/scroll-down-freezed-cursor)

(defun my/scroll-up-freezed-cursor (&optional n)
	"Scroll up the buffer by 1 row keeping the cursor on the same line.
If called with the N argument scroll up by N rows. It can be called
interactively with C-u or M-[number]"
	(interactive "p")
	(let ((number-of-lines (or n 1)))
		(scroll-up number-of-lines)
		(forward-line number-of-lines)))

(global-set-key (kbd "S-M-<prior>") 'my/scroll-up-freezed-cursor)

(defun open-emacsinit.org ()
	"Quickly open the org version of the configuration file."
	(interactive)
	(find-file "~/repos/emacs-config/emacsinit.org"))

(global-set-key (kbd "<f5>") 'open-emacsinit.org)

(defun my/reload-emacs-init-file ()
	"Quickly load the configuration file."
	(interactive)
	(let ((buf (buffer-name (current-buffer))))
		(load-file user-init-file)
		(switch-to-buffer buf)))

(global-set-key (kbd "C-<f5>") 'my/reload-emacs-init-file)

(global-set-key "\C-cr" 'query-replace-regexp)

(global-set-key (kbd "RET") 'newline-and-indent)

(defun tidy-xml ()
	"Tidies the XML content in the buffer using `tidy'"
	(interactive)
	(shell-command-on-region
		(point-min) (point-max)
		"tidy -i -w 72 -q -xml"
		(current-buffer)
		t
		"*Tidy Error*"
		t))

(eval-after-load "nxml-mode"
	(lambda ()
		(define-key nxml-mode-map "\C-ct" 'tidy-xml)))

(setq initial-files (expand-file-name "initial-files.el" user-emacs-directory))
(load initial-files)

;;; End of init.el
